import { useEffect, useState } from "react";
import "./Title.css";

export const Title = () => {

	const [isSmaller, setIsSmaller] = useState(false);
	const titleChange = () => {
		if (!isSmaller) {
			setIsSmaller(true);
		};
	};

	useEffect(() => {
		if (!isSmaller) {
			window.addEventListener('scroll', titleChange);
		}
		return () => {
			window.removeEventListener('scroll', titleChange);
		}
	});

	return (
		<h1 className={isSmaller?"smaller":""}>
			Opening <br />
			Sign <br />
			<i className="fas fa-hourglass-half"></i>
		</h1>
	);
}
