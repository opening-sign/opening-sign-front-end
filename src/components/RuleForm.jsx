import { useEffect, useState } from "react";
import { weekDays } from "opening-sign-shifts-to-schedule";

import "./RuleForm.css";

export const RuleForm = (props) => {
	const defaultRule = props.rule ? props.rule : {};
	const [rule, setRule] = useState({
		...{
			repeating: "weekly",
			repeatingDates: ["mon", "tue", "thu", "fri"],
			closed: false,
			shift: [28800, 64800], // 08:00 - 18:00
		},
		...defaultRule
	});
	useEffect(() => {
		props.rulesChanger(rule);
	});

	return <form className="RuleForm glass">
		<WeekDaysCheckboxes rule={rule} setRule={setRule} />
		<ShiftInputs rule={rule} setRule={setRule} />
		<button
			className="glass"
			style={{ color: "hsla(0, 0%, 100%, 0.5)", borderColor: "initial", backgroundColor: "hsla(0, 100%, 50%, 0.2)" }}
			onClick={(e) => {
				e.preventDefault();
				props.rulesChanger(undefined);
			}}
		>
			remove
		</button>
	</form>
}

const WeekDaysCheckboxes = (props) => {
	const rule = props.rule;
	const setRule = props.setRule;

	let activeDays = [];

	return <div className="WeekDaysCheckboxes">
		{weekDays.map((day, index) => {

			if (rule.repeatingDates.includes(day)) {
				activeDays[index] = day;
			}

			return <label key={index}>
				<input type="checkbox" defaultChecked={rule.repeatingDates.includes(day)} onChange={(e) => {
					activeDays[index] = e.target.checked ? day : undefined;
					setRule(oldRule => { return { ...oldRule, repeatingDates: activeDays.filter(x => x !== undefined) } })
				}} />
				{day}&nbsp;
			</label>
		})}
	</div>
};

const ShiftInputs = (props) => {
	const rule = props.rule;
	const setRule = props.setRule;

	function hhmmssToSeconds(hhmmss) {
		let split = hhmmss.split(':');
		return (+split[0]) * 60 * 60 + (+split[1]) * 60 + (+split[2]);
	}

	function secondsToHhmmss(seconds) {
		let hhmmss = new Date(seconds * 1000).toISOString().substr(11, 8);
		// console.log(seconds + " formatted = " + hhmmss);
		return hhmmss;
	}

	let activeShift = rule.shift;

	return <div className="Shift">
		<label>
			<span>Starts at </span><br />
			<input className="glass" type="time" defaultValue={secondsToHhmmss(rule.shift[0])} required={true} onChange={(e) => {
				activeShift[0] = hhmmssToSeconds(e.target.value);
				console.log(activeShift);
				setRule(oldRule => { return { ...oldRule, shift: activeShift } })
			}} />
		</label>
		<label>
			<span>Ends at </span><br />
			<input className="glass" type="time" defaultValue={secondsToHhmmss(rule.shift[1])} required={true} onChange={(e) => {
				activeShift[1] = hhmmssToSeconds(e.target.value);
				console.log(activeShift);
				setRule(oldRule => { return { ...oldRule, shift: activeShift } })
			}} />
		</label>
	</div>;
};
