import { useState } from 'react';
import "./RulesManager.css";
import { RuleForm } from './RuleForm';

export const RulesManager = (props) => {
	const [rules, setRules] = useState([]);

	const RulesMapper = () => {
		return rules.map((rule, index) => {
			return <RuleForm
				key={index}
				rule={rule}
				rulesChanger={
					(newRule) => setRules(oldRules => {
						let newRules = oldRules;
						if (newRule) {
							console.log("rule was updated")
							newRules[index] = newRule;
						} else {
							console.log("rule was not updated")
							newRules.splice(index, 1);
						}
						console.log(newRules)
						return newRules;
					})
				}
			/>
		})
	};

	// console.log("====RULES====", rules);

	return (
		<div className="RulesManager">
			<button className="glass" onClick={() => setRules(oldArray => [...oldArray, {edit: true}])}>
				<span>
					Add a rule
				</span>
				<i className="fa-regular fa-calendar-plus"></i>
			</button>
			<RulesMapper/>
		</div>
	);
}
