import './App.css';
import { schedule } from "opening-sign-shifts-to-schedule";
import { Title } from "components/Title";
import { RulesManager } from "components/RulesManager";

function App() {
  console.log(schedule([
    {
      repeating: "weekly",
      repeatingDates: ["mon"],
      closed: false,
      shift: [28800, 64800],
    },
  ]));

  return (
    <>
      <Title/>
      <main className="glass">
        <p>Update your store hours from one place</p>
        <div className="two-cols">
          <div>
            <h2>Rules</h2>
            <RulesManager/>
          </div>
          <div>
            <h2>Preview</h2>
          </div>
        </div>
      </main>
    </>
  );
}

export default App;
